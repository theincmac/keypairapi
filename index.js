const dotenv = require('dotenv').config()
const cors = require('cors')
const ZR = require('./zenroomHelper');

const express = require('express')
const app = express()
const port = process.env.PORT || 3000;

console.log("\n\ House.coop Keypair server \n\n")

const keygen_contract = `rule check version 1.0.0
Scenario 'simple': Create the keypair
Given that I am known as 'Admin'
When I create the keypair
Then print my data`


var whitelist = [
	'http://housecoop.org', 
	'http://housecoop.org'
]

var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}

app.use(cors(corsOptions))


app.get('/', (req, res) => {

	const zr = new ZR();

	zr.runScript({script: keygen_contract})
	.then((_keys) => {
		let keys = {
			'ADMIN_PUBLIC_KEY' : _keys.Admin.keypair.public_key,
			'ADMIN_PRIVATE_KEY' : _keys.Admin.keypair.private_key
		};
		res.json(keys);
	
	})
	.catch(err => {
		console.log(err);
		res.status(500).json({err: err});
	})

});



app.listen(port, () => console.log(`Listening on port ${port}`))

